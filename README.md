# Cosyphp Framework (Kernel)

> **Note:** This repository contains the core code of the Cosyphp framework. If you want to build an application using Cosyphp, visit the main [Cosyphp repository](https://github.com/cosyphp/cosyphp).

## Cosyphp PHP Framework

Cosyphp is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Cosyphp attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Official Documentation

Documentation for the framework can be found on the [Lumen-5.5 docs](https://learnku.com/docs/lumen/5.5).

## License

The Cosyphp framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
